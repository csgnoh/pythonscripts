# TODO Read states of Malaysia from file at runtime, only fall back to internally defined list if file not found

__author__ = 'gnohc'
import pickle


states_of_malaysia = {'Perlis':'Kangar','Kedah':'Alor Star','Perak':'Ipoh','Selangor':'Shah Alam'}

print(states_of_malaysia['Perlis'].upper())

print('Number of states in Malaysia is '+str(len(states_of_malaysia)))
for state in states_of_malaysia:
    print('The state capital of '+state+' is '+states_of_malaysia[state])

# save dictionary to file using pickle method
f = open('mypickledict.txt','wb')
pickle._dump(states_of_malaysia,f)
f.close()

# load dictionary from file
g = open('mypickledict.txt','rb')
my_retrieved_states = pickle.loads(g.read())

print('Number of states in Malaysia is '+str(len(my_retrieved_states)))
for my_state in my_retrieved_states:
    print('The state capital of '+my_state+' is '+my_retrieved_states[my_state])




