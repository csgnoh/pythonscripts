import random
import os
import sys

# comment
def genRandomString(strLength):
    alphabet_soup = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    soup_len = len(alphabet_soup)
    randomString = ''

    for i in range(1,strLength):
        randomString = randomString + str(alphabet_soup[random.randint(1,soup_len-1)])

    return randomString



# write random strings to a file
g = open('random.txt','w')
for j in range(1,100):
    g.write(genRandomString(100) + '\n')
g.close()


# open a file and print each line in file
f = open('random.txt','r')
for line_in_file in f:
    # using end='' removes newline character
    print(line_in_file,end='')
f.close()








