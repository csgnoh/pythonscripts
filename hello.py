def say_hello():
    print("Hello")


def add_two_numbers(num1, num2):
    return int(num1)+int(num2)


say_hello()
firstInput = input('Enter first number:')
secondInput = input('Enter second number:')
print(add_two_numbers(firstInput, secondInput))
for x in range(0, 9):
    print('Value of x is '+str(x))

# comment
my_fruits = ['banana', 'orange', 'apple', 'strawberry']
my_fruits.append('grape')
for fruit in my_fruits:
    print('The name of this fruit is '+fruit)
my_fruits.remove('banana')
print('\n\n\n')
for fruit in my_fruits:
    print('The name of this fruit is '+fruit)

