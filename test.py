try:
    with open("test.cmd") as f:
        content = f.read()
except FileNotFoundError:
    print("test.cmd file is missing")
except PermissionError:
    print("You are not allowed to read test.cmd")